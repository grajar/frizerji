CREATE DATABASE  IF NOT EXISTS `frizerji` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `frizerji`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: frizerji
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `opening_times`
--

DROP TABLE IF EXISTS `opening_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opening_times` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `saloon_id` int(10) unsigned NOT NULL,
  `open` time NOT NULL,
  `close` time NOT NULL,
  `day` int(11) NOT NULL,
  PRIMARY KEY (`id`,`saloon_id`),
  KEY `fk_opening_times_saloons1` (`saloon_id`),
  CONSTRAINT `fk_opening_times_saloons1` FOREIGN KEY (`saloon_id`) REFERENCES `saloons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opening_times`
--

LOCK TABLES `opening_times` WRITE;
/*!40000 ALTER TABLE `opening_times` DISABLE KEYS */;
INSERT INTO `opening_times` VALUES (1,1,'07:00:00','21:00:00',0),(2,1,'07:00:00','21:00:00',1),(3,1,'07:00:00','21:00:00',2),(4,1,'07:00:00','21:00:00',3),(5,1,'07:00:00','21:00:00',4),(6,1,'07:00:00','21:00:00',5),(7,1,'07:00:00','21:00:00',6);
/*!40000 ALTER TABLE `opening_times` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-03-05 13:44:48
