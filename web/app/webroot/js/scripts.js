$(document).ready(function()
{
	/**
	 * Form validation
	 */
	var inputs = $('.input.text');
	var inputFields = inputs.children('input');
	
	// Input field blur function
	$('.container').delegate('.input input:not(.no-validation)', 'blur', function(){ // Add class "no-validation" to field to disable validation; NOTE: js mam tuki za ponovit geslo, moram spremenit v tisto za datum konca izmene;
		/**
		 * Do Ajax POST
		 */
		var element = $(this);
		var patt1 = new RegExp(/\[([a-zA-Z_]*?)\](\[\d\])?\[([a-zA-Z_]*)\]/);
		var matched = patt1.exec(element.attr('name'));
		
		var controller = matched[1];
		var fieldName = matched[matched.length - 1];
		
		var f2 = null;
		var v2 = '';
		if (fieldName == 'password_confirm')
		{
			f2 = 'password';
			v2 = $('.input.password input#User0Password').val();
		}
		
		$.post("/" + controller + 's/validate_form', {
			field : fieldName, value : element.val(), field2 : f2, value2 : v2},
			function(data){
				handleResponce(data, element);
			}
		);
	});
	
	function handleResponce(error, element)
	{
		var id = element.attr('id');
		var errMsg = $('#' + id + '-form-error');
		if (error.length > 0) {
			if (errMsg.length === 0)
			{
				element.attr('class', 'form-error');
				$('<div id="' + id  + '-form-error" class="error-message" style="display:none;">'+ error +'</div>').insertAfter(element).slideDown(200);
			}
			else if (errMsg.html() != error)
			{
				errMsg.fadeOut(200, function(){
					$(this).html(error).fadeIn(200);
				});
			}
		}
		else if (errMsg.length > 0)
		{
			element.attr('class', '');
			$('#' + id + '-form-error').slideUp(200, function(){ $(this).remove(); });
		}
	}
});