$(document).ready(function()
{
	$( ".selectable" ).selectable({
      stop: function() {
        var result = '';
        $( ".ui-selected", this ).each(function() {
          var index = $( ".selectable li" ).index( this );
          result = result + " #" + ( index );
        });
        var day = $(this).attr("day");
        alert(result + ' ' + day);
      }
    });
});