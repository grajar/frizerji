<?php
App::uses('AppModel', 'Model');
/**
 * Appointment Model
 *
 * @property Shift $Shift
 * @property ServiceArchive $ServiceArchive
 * @property Customer $Customer
 */
class Appointment extends AppModel {

	public $displayField = 'service_archive_id';
	public $actsAs = array('containable');

	public $validate = array(
		'time' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Napačna oblika zapisa',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'created' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Napačna oblika zapisa',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'modified' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Napačna oblika zapisa',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'time' => array(
			'isInShift' => array(
				'rule' => array('isInShift'),
				'message' => 'Čas naročila ni znotraj izbrane izmene'
			),
			'isOverlaping' => array(
				'rule'=>'isOverlaping',
				'message' => 'Naročilo se časovno prekriva z drugim naročilom te izmene')
		),
	);

	// preveri če naročilo časovno pade v izbrano izmeno
	public function isInShift()
	{
		$shift = $this->Shift->find('first', array(
			'conditions' => array(
				'Shift.id' => $this->data[$this->alias]['shift_id'],
			),
			'fields' => array('start', 'stop'),
			'recursive' => -1
		));
		
		$appointmentBoundries = $this->_getAppointmentBoundries($this->data);
		
		$appointmentStart = $appointmentBoundries['start'];
		$appointmentEnd = $appointmentBoundries['stop'];

		$shiftStart = $shift['Shift']['start'];
		$shiftEnd = $shift['Shift']['stop'];

		if($shiftStart <= $appointmentStart && $shiftEnd >= $appointmentEnd)
		{
			return true;
		}

		return false;
	}

	// preveri če se naročilo prekriva s kakim drugim v tej izmeni
	public function isOverlaping()
	{
		$appointments = $this->find('all', array(
			'conditions' => array(
				'Appointment.shift_id' => $this->data[$this->alias]['shift_id'],
			),
		));

		$boundries1 = $this->_getAppointmentBoundries($this->data);
		foreach ($appointments as $appointment)
		{
			if (!isset($this->data[$this->alias]['id']) || $appointment[$this->alias]['id'] != $this->data[$this->alias]['id'])
			{
				$boundries2 = $this->_getAppointmentBoundries($appointment);
				if ($boundries1['stop'] > $boundries2['start'] && $boundries1['start'] < $boundries2['stop'])
				{
					return false;
				}
			}	
		}
		return true;
	}

	// iz Appointmenta in ServiceArchive izračuna kdaj se začne in konča izbrano naročilo
	public function _getAppointmentBoundries($appointment)
	{
		$service = $this->ServiceArchive->find('first', array(
			'conditions' => array(
				'ServiceArchive.id' => $appointment[$this->alias]['service_archive_id']
			),
			'fields' => array('HOUR(duration) AS h' , 'MINUTE(duration) AS i', 'SECOND(duration) AS s'),
			'recursive' => -1
		));
		$duration = DateInterval::createFromDateString(
			$service[0]['h'].' hours + '.
			$service[0]['i'].' minutes + '.
			$service[0]['s'].' seconds');

		$appointmentDT = new DateTime($appointment[$this->alias]['time']); 
		$appointmentStart = $appointmentDT->format("Y-m-d H:i:s");

		$appointmentDT->add($duration);
		$appointmentEnd = $appointmentDT->format("Y-m-d H:i:s");

		return array('start'=>$appointmentStart, 'stop'=> $appointmentEnd);
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Shift' => array(
			'className' => 'Shift',
			'foreignKey' => 'shift_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ServiceArchive' => array(
			'className' => 'ServiceArchive',
			'foreignKey' => 'service_archive_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
