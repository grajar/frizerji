<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Saloon $Saloon
 * @property Shift $Shift
 */
class User extends AppModel {

	public function beforeSave($options = array())
	{
        if (isset($this->data[$this->alias]['password']))
        {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }

        return true;
   }

   public function notInCustomers()
   {
   	return true; // TODO: dokončaj :)
   }

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = array('containable');

	public $validate = array(
		'username' => array(
			'minlength' => array(
				'rule' => array('minlength', 3),
				'message' => 'Uporabniško ime mora vsebovati vsaj 3 znake',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'Uporabniško ime že obstaja'
			),
			'notInCustomers' => array(
				'rule' => array('notInCustomers'),
				'message' => 'Uporabniško ime že obstaja')
		),
		'password' => array(
			'minlength' => array(
				'rule' => array('minlength', 2),
				'message' => 'Geslo mora vsebovati vsaj 8 znakov',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'minlength' => array(
				'rule' => array('minlength', 1),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'color' => array(
			'hex' => array(
				'custom' => array('/^[0-9a-f]{6}$/i'),
				'message' => 'Barva mora vsebovati 6 hexadecimalnih znakov',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'created' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Napačna oblika zapisa',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'modified' => array(
			'datetime' => array(
				'rule' => array('datetime'),
				'message' => 'Napačna oblika zapisa',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Saloon' => array(
			'className' => 'Saloon',
			'foreignKey' => 'saloon_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Shift' => array(
			'className' => 'Shift',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
