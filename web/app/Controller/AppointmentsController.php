<?php
App::uses('AppController', 'Controller');
/**
 * Appointments Controller
 *
 * @property Appointment $Appointment
 */
class AppointmentsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->Appointment->recursive = 0;
		$this->set('appointments', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->Appointment->id = $id;
		if (!$this->Appointment->exists())
		{
			throw new NotFoundException(__('Invalid appointment'));
		}
		$this->set('appointment', $this->Appointment->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Appointment->create();
			if ($this->Appointment->save($this->request->data))
			{
				$this->Session->setFlash(__('The appointment has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The appointment could not be saved. Please, try again.'));
			}
		}
		$shifts = $this->Appointment->Shift->find('list'); 
		$serviceArchives = $this->Appointment->ServiceArchive->find('list');
		$customers = $this->Appointment->Customer->find('list');
		$this->set(compact('shifts', 'serviceArchives', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->Appointment->id = $id;
		if (!$this->Appointment->exists())
		{
			throw new NotFoundException(__('Invalid appointment'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Appointment->save($this->request->data))
			{
				$this->Session->setFlash(__('The appointment has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The appointment could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Appointment->read(null, $id);
		}
		$shifts = $this->Appointment->Shift->find('list');
		$serviceArchives = $this->Appointment->ServiceArchive->find('list');
		$customers = $this->Appointment->Customer->find('list');
		$this->set(compact('shifts', 'serviceArchives', 'customers'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Appointment->id = $id;
		if (!$this->Appointment->exists())
		{
			throw new NotFoundException(__('Invalid appointment'));
		}
		if ($this->Appointment->delete())
		{
			$this->Session->setFlash(__('Appointment deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Appointment was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->Appointment->recursive = 0;
		$this->set('appointments', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->Appointment->id = $id;
		if (!$this->Appointment->exists())
		{
			throw new NotFoundException(__('Invalid appointment'));
		}
		$this->set('appointment', $this->Appointment->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post'))
		{
			$this->Appointment->create();
			if ($this->Appointment->save($this->request->data)) {
				$this->Session->setFlash(__('The appointment has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The appointment could not be saved. Please, try again.'));
			}
		}
		$shifts = $this->Appointment->Shift->find('list');
		$serviceArchives = $this->Appointment->ServiceArchive->find('list');
		$customers = $this->Appointment->Customer->find('list');
		$this->set(compact('shifts', 'serviceArchives', 'customers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->Appointment->id = $id;
		if (!$this->Appointment->exists())
		{
			throw new NotFoundException(__('Invalid appointment'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Appointment->save($this->request->data))
			{
				$this->Session->setFlash(__('The appointment has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The appointment could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Appointment->read(null, $id);
		}
		$shifts = $this->Appointment->Shift->find('list');
		$serviceArchives = $this->Appointment->ServiceArchive->find('list');
		$customers = $this->Appointment->Customer->find('list');
		$this->set(compact('shifts', 'serviceArchives', 'customers'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Appointment->id = $id;
		if (!$this->Appointment->exists())
		{
			throw new NotFoundException(__('Invalid appointment'));
		}
		if ($this->Appointment->delete())
		{
			$this->Session->setFlash(__('Appointment deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Appointment was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
