<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');
App::uses('FireCake', 'DebugKit.Lib'); 

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
		'Security',
		'Session',
		'RequestHandler',
		'Auth' => array(
			'loginRedirect' => array('controller' => 'saloons', 'action' => 'calendar', 'firstDay'=>'2013-02-16', 'saloon' => 1, 'numDays'=>5),
			'logoutRedirect' => array('controller' => 'saloons', 'action' => 'calendar', 'firstDay'=>'2013-02-16', 'saloon' => 1, 'numDays'=>5),
			'loginAction' => array('controller' => 'users', 'action' => 'login')
		),
		'DebugKit.Toolbar' => array('forceEnable' => true)
	);

	public $helpers = array('Html', 'Form', 'Js');

	public function beforeFilter()
	{
		$this->Auth->userModel = 'User';

		$this->Session->write('Config.language', 'slv');
		
		// Web access 
		$this->_access();
		
		// Local / public server
		$this->_server();
		
		// Blackhole callback action
		$this->Security->blackHoleCallback = '_renew';
		
		// Publicly allowed actions
		$this->Auth->allow('display', 'register', 'activate', 'validate_form', 'index', 'view', 'add', 'edit', 'login', 'delete', 'calendar'); 

		// DebugKit SQL explain action
		if (Configure::read('thisServer') == 'local')
		{
			$this->Auth->allow('sql_explain');
		}
		
		// Disable form security if Ajax request
		if ($this->RequestHandler->isAjax())
		{
			$this->Security->validatePost = false;
			$this->Security->csrfCheck = false;
		}
	}

	/**
	 * Checks if local or live server, saves different URL parts to configuration
	 */
	function _server() 
	{ 
		/* Domain, subdomain, web address */
		$url_parts = explode('.', FULL_BASE_URL);
		
		$i = 3;
		$subdomain = '';
		
		if (isset($url_parts[count($url_parts) - 3]))
		{
			while(isset($url_parts[count($url_parts) - $i]))
			{
				$subdomain = strtolower(trim($this->_removeHttp($url_parts[count($url_parts) - $i]))) . $subdomain;
				$i++;
			}
		}
		else 
		{
			$url_parts[count($url_parts) - 2] = $this->_removeHttp($url_parts[count($url_parts) - 2]);
		}
		
		Configure::write("subdomain", $subdomain);
		
		Configure::write("DomainName", strtolower(trim($url_parts[count($url_parts) - 2]) . "." . trim($url_parts[count($url_parts) - 1])));
		Configure::write("DomainExtension", strtolower(trim($url_parts[count($url_parts) - 1])));
	
		if (Configure::read('DomainExtension') == 'lc' || Configure::read('DomainExtension') == 'lc:8888')
		{
			Configure::write('thisServer', 'local');
		}
		else 
		{
			Configure::write('thisServer', 'live');
		}

		Configure::write('publicSite', 'http://www.' . Configure::read('DomainName'));
		
		if($subdomain === '') {
			$this->redirect(Configure::read('publicSite')); // TOOD: Redirect status 301
		}
	}
	
	/**
	 * Reload page
	 * @return action redirect
	 */
	function _renew() 
	{
		$this->redirect(FULL_BASE_URL . $this->here);
	}
	
	/**
	 * Access controll 
	 */
	function _access() 
	{
		$allow = array('127.0.0.1'); 
		
		if (in_array($_SERVER['REMOTE_ADDR'], $allow) || $this->Auth->user('username') == 'bostjan@pisler.si' || $this->Auth->user('username') == 'gorazd.rajar@gmail.com') 
		{            
			if ($this->Auth->user('username') == 'bostjan@pisler.si' || $this->Auth->user('username') == 'gorazd.rajar@gmail.com' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1') {
				Configure::write('debug', 2);
			}

			Configure::write('excludeAnalytics', true);
			Configure::write('Cache.disable', true);
		} 
		else {
			Configure::write('debug', 0);
			Configure::write('Cache.disable', false);
			Configure::write('excludeAnalytics', false);
			header("location: ADDRESS");
			exit();
		}
	}
	
	/**
	 * Slug helper TODO: move slug helper to helpers?
	 * @param  string $string string to slug-ify
	 * @return string         stripped string
	 */
	function _slug($string) 
	{
		return Inflector::slug(utf8_encode(str_replace(array('č', 'š', 'ž'), array('c', 's', 'z'), strtolower($string))), '-');
	}
	
	/**
	 * [_removeHttp description]
	 * @param  [type] $url [description]
	 * @return [type]      [description]
	 */
	function _removeHttp($url) 
	{
		$disallowed = array('http://', 'https://');
		foreach($disallowed as $d)
		{
			if(strpos($url, $d) === 0) {
				return str_replace($d, '', $url);
			}
		}
		return $url;
	}
	
	/**
	 * Ajax form validation
	 * @return array validation data
	 */
	public function ajax_validate_form() 
	{
		if ($this->RequestHandler->isAjax()) 
		{
			$this->request->data[$this->modelClass][$this->request->data['field']] = $this->request->data['value'];
			
			// Multy field dependant validation 
			// FireCake::log($this->request->data);
			$this->request->data['field2'] != 'null' ? $this->request->data[$this->modelClass][$this->request->data['field2']] = $this->request->data['value2'] : "";
			
			$this->{$this->modelClass}->set($this->request->data);
			
			if ($this->{$this->modelClass}->validates()) 
			{
				$this->autoRender = false;
			} 
			else 
			{
				// FireCake::log($this->request->data['field']);
				$error = $this->validateErrors($this->{$this->modelClass});
				if (isset($error[$this->request->data['field']][0]))
				{
					$this->set('error', $error[$this->request->data['field']][0]);
					$error[$this->request->data['field']][0];
					$this->render('/Ajax/formError');
				}
			}
		}
	}
}