<?php
App::uses('AppController', 'Controller');
/**
 * OpeningTimes Controller
 *
 * @property OpeningTime $OpeningTime
 */
class OpeningTimesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->OpeningTime->recursive = 0;
		$this->set('openingTimes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->OpeningTime->id = $id;
		if (!$this->OpeningTime->exists())
		{
			throw new NotFoundException(__('Invalid opening time'));
		}
		$this->set('openingTime', $this->OpeningTime->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->OpeningTime->create();
			if ($this->OpeningTime->save($this->request->data))
			{
				$this->Session->setFlash(__('The opening time has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The opening time could not be saved. Please, try again.'));
			}
		}
		$saloons = $this->OpeningTime->Saloon->find('list');
		$this->set(compact('saloons'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null)
	{
		$this->OpeningTime->id = $id;
		if (!$this->OpeningTime->exists())
		{
			throw new NotFoundException(__('Invalid opening time'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->OpeningTime->save($this->request->data)) {
				$this->Session->setFlash(__('The opening time has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The opening time could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->OpeningTime->read(null, $id);
		}
		$saloons = $this->OpeningTime->Saloon->find('list');
		$this->set(compact('saloons'));
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->OpeningTime->id = $id;
		if (!$this->OpeningTime->exists())
		{
			throw new NotFoundException(__('Invalid opening time'));
		}
		if ($this->OpeningTime->delete())
		{
			$this->Session->setFlash(__('Opening time deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Opening time was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->OpeningTime->recursive = 0;
		$this->set('openingTimes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->OpeningTime->id = $id;
		if (!$this->OpeningTime->exists())
		{
			throw new NotFoundException(__('Invalid opening time'));
		}
		$this->set('openingTime', $this->OpeningTime->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post'))
		{
			$this->OpeningTime->create();
			if ($this->OpeningTime->save($this->request->data))
			{
				$this->Session->setFlash(__('The opening time has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The opening time could not be saved. Please, try again.'));
			}
		}
		$saloons = $this->OpeningTime->Saloon->find('list');
		$this->set(compact('saloons'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->OpeningTime->id = $id;
		if (!$this->OpeningTime->exists())
		{
			throw new NotFoundException(__('Invalid opening time'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->OpeningTime->save($this->request->data))
			{
				$this->Session->setFlash(__('The opening time has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The opening time could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->OpeningTime->read(null, $id);
		}
		$saloons = $this->OpeningTime->Saloon->find('list');
		$this->set(compact('saloons'));
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->OpeningTime->id = $id;
		if (!$this->OpeningTime->exists())
		{
			throw new NotFoundException(__('Invalid opening time'));
		}
		if ($this->OpeningTime->delete())
		{
			$this->Session->setFlash(__('Opening time deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Opening time was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
