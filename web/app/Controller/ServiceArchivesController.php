<?php
App::uses('AppController', 'Controller');
/**
 * ServiceArchives Controller
 *
 * @property ServiceArchive $ServiceArchive
 */
class ServiceArchivesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index()
	{
		$this->ServiceArchive->recursive = 0;
		$this->set('serviceArchives', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null)
	{
		$this->ServiceArchive->id = $id;
		if (!$this->ServiceArchive->exists())
		{
			throw new NotFoundException(__('Invalid service archive'));
		}
		$this->set('serviceArchive', $this->ServiceArchive->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->ServiceArchive->create();
			if ($this->ServiceArchive->save($this->request->data))
			{
				$this->Session->setFlash(__('The service archive has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The service archive could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ServiceArchive->id = $id;
		if (!$this->ServiceArchive->exists())
		{
			throw new NotFoundException(__('Invalid service archive'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->ServiceArchive->save($this->request->data))
			{
				$this->Session->setFlash(__('The service archive has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The service archive could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->ServiceArchive->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->ServiceArchive->id = $id;
		if (!$this->ServiceArchive->exists())
		{
			throw new NotFoundException(__('Invalid service archive'));
		}
		if ($this->ServiceArchive->delete())
		{
			$this->Session->setFlash(__('Service archive deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Service archive was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index()
	{
		$this->ServiceArchive->recursive = 0;
		$this->set('serviceArchives', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null)
	{
		$this->ServiceArchive->id = $id;
		if (!$this->ServiceArchive->exists())
		{
			throw new NotFoundException(__('Invalid service archive'));
		}
		$this->set('serviceArchive', $this->ServiceArchive->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add()
	{
		if ($this->request->is('post'))
		{
			$this->ServiceArchive->create();
			if ($this->ServiceArchive->save($this->request->data))
			{
				$this->Session->setFlash(__('The service archive has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The service archive could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null)
	{
		$this->ServiceArchive->id = $id;
		if (!$this->ServiceArchive->exists()) 
		{
			throw new NotFoundException(__('Invalid service archive'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->ServiceArchive->save($this->request->data))
			{
				$this->Session->setFlash(__('The service archive has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The service archive could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->ServiceArchive->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->ServiceArchive->id = $id;
		if (!$this->ServiceArchive->exists())
		{
			throw new NotFoundException(__('Invalid service archive'));
		}
		if ($this->ServiceArchive->delete())
		{
			$this->Session->setFlash(__('Service archive deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Service archive was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
