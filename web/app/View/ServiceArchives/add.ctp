<div class="serviceArchives form">
<?php echo $this->Form->create('ServiceArchive'); ?>
	<fieldset>
		<legend><?php echo __('Add Service Archive'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('duration');
		echo $this->Form->input('price');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Service Archives'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Appointments'), array('controller' => 'appointments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Appointment'), array('controller' => 'appointments', 'action' => 'add')); ?> </li>
	</ul>
</div>
