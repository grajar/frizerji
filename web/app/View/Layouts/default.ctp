<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	
	<title><?php echo (isset($title_for_layout) ?$title_for_layout . ' | ' : "") . __('Frizerji so zakon :)'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- TODO: touch icons -->
	<!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png"> -->
		
	<?php
	echo $this->Html->meta('description', $page_description);
	echo $this->Html->meta('keywords', $page_keywords);
	
	echo $this->Html->css(array('bootstrap.min', 'style', $style_for_layout));
	?>
	
	<?php // echo $this->element('google_analytics'); // TODO user specific analytics ?>
</head>
<noscript><?php echo __('POMOMBNO: Javascript mora biti vklopljen za pravilno delovanje strani!') // TODO: test noscript ?></noscript>
<body class="<?php if (isset($class_for_layout) && !empty($class_for_layout)) echo $class_for_layout; ?>">

<!-- Header start -->
<div class="navbar navbar-static-top head">
	<?php if (!empty($head_for_layout))
	{
		echo $this->element($head_for_layout);
	} ?>
	<div class="container">
		<?php if(!empty($menu_for_layout))
		{
			echo $this->element($menu_for_layout);
		} ?>
	</div>
</div>
<!-- Header end -->

<!-- Content start -->
<div class="container content" >
	<?php echo $this->fetch('content'); ?>
	<!-- Right menu -->
</div>
<!-- Content end -->

<div class="clearfix">	</div>

<!-- Footer start -->
<?php if(!empty($footer_for_layout)) { ?>
	<div class="footer-wrapper">
		<div class="footer container">
			<?php echo $this->element($footer_for_layout); ?>
		</div>
	</div>
<?php } ?>
</div>

<!-- Scripts start -->
<?php 
$scripts = null;

if (Configure::read('thisServer') == 'local') {
  $scripts[] = 'jquery-1.9.1.min';
  $scripts[] = 'jquery-ui-1.10.0.custom.min.js';
}
else {
  $scripts[] = '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js';
  $scripts[] = '//code.jquery.com/ui/1.10.1/jquery-ui.js';
}

if (isset($view_scripts) && !empty($view_scripts)) $scripts[] = $view_scripts;

$scripts[] = array('bootstrap.min', 'scripts', 'public.scripts');

echo $this->Html->script($scripts);

if($this->fetch('scripts')){
  echo $this->fetch('scripts');
}

if($this->fetch('inlineScripts')){
  echo $this->fetch('inlineScripts');
}

// Print javascript
echo $this->Js->writeBuffer(array('cache' => true));

if(!empty($this->data)){
	debug($this->data);
}
?>
</body>
</html>