<?php 
echo $this->element('calendar', array('calendarData' => $calendarData, 'dates' => $dates));
echo debug($employees);
?>

<div id="add_shift">
	<?php echo $this->Form->create('Shift', array('action' => 'addShift')); ?>
	<fieldset>
		<legend><?php echo __('Nova izmena'); ?></legend>
	<?php
		echo $this->Form->input('employee_id');
		echo $this->Form->input('start', array('type' => 'datetime', 'interval' => 30, 'separator' => '', /*'class'=> */ )); 
		echo $this->Form->input('stop', array('type' => 'time', 'interval' => 30, 'separator' => '', /*'class'=> */ )); 
	?>
	</fieldset>
	<?php echo $this->Form->submit(__('Dodaj'), array('controller' => 'saloons', 'action' => 'addShift')); ?>	
	<?php echo $this->Form->end();?>	
</div>

<?php echo $this->Session->flash(); ?>