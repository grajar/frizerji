<div class="saloons form">
<?php echo $this->Form->create('Saloon'); ?>
	<fieldset>
		<legend><?php echo __('Edit Saloon'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('interval');
		echo $this->Form->input('address');
		echo $this->Form->input('city');
		echo $this->Form->input('phone');
		echo $this->Form->input('active');
		echo $this->Form->input('latitude');
		echo $this->Form->input('longitude');
		echo $this->Form->input('logo');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Saloon.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Saloon.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Saloons'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Services'), array('controller' => 'services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service'), array('controller' => 'services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Opening Times'), array('controller' => 'opening_times', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Opening Time'), array('controller' => 'opening_times', 'action' => 'add')); ?> </li>
	</ul>
</div>
