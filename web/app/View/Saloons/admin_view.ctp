<div class="saloons view">
<h2><?php  echo __('Saloon'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Interval'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['interval']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Latitude'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['latitude']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Longitude'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['longitude']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Logo'); ?></dt>
		<dd>
			<?php echo h($saloon['Saloon']['logo']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Saloon'), array('action' => 'edit', $saloon['Saloon']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Saloon'), array('action' => 'delete', $saloon['Saloon']['id']), null, __('Are you sure you want to delete # %s?', $saloon['Saloon']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Saloons'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saloon'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Services'), array('controller' => 'services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service'), array('controller' => 'services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Opening Times'), array('controller' => 'opening_times', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Opening Time'), array('controller' => 'opening_times', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Users'); ?></h3>
	<?php if (!empty($saloon['User'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Saloon Id'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Surname'); ?></th>
		<th><?php echo __('Active'); ?></th>
		<th><?php echo __('Color'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($saloon['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['saloon_id']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['password']; ?></td>
			<td><?php echo $user['name']; ?></td>
			<td><?php echo $user['surname']; ?></td>
			<td><?php echo $user['active']; ?></td>
			<td><?php echo $user['color']; ?></td>
			<td><?php echo $user['created']; ?></td>
			<td><?php echo $user['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), null, __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Services'); ?></h3>
	<?php if (!empty($saloon['Service'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Saloon Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Duration'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Visible'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($saloon['Service'] as $service): ?>
		<tr>
			<td><?php echo $service['id']; ?></td>
			<td><?php echo $service['saloon_id']; ?></td>
			<td><?php echo $service['name']; ?></td>
			<td><?php echo $service['duration']; ?></td>
			<td><?php echo $service['price']; ?></td>
			<td><?php echo $service['visible']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'services', 'action' => 'view', $service['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'services', 'action' => 'edit', $service['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'services', 'action' => 'delete', $service['id']), null, __('Are you sure you want to delete # %s?', $service['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Service'), array('controller' => 'services', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Opening Times'); ?></h3>
	<?php if (!empty($saloon['OpeningTime'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Saloon Id'); ?></th>
		<th><?php echo __('Open'); ?></th>
		<th><?php echo __('Close'); ?></th>
		<th><?php echo __('Day'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($saloon['OpeningTime'] as $openingTime): ?>
		<tr>
			<td><?php echo $openingTime['id']; ?></td>
			<td><?php echo $openingTime['saloon_id']; ?></td>
			<td><?php echo $openingTime['open']; ?></td>
			<td><?php echo $openingTime['close']; ?></td>
			<td><?php echo $openingTime['day']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'opening_times', 'action' => 'view', $openingTime['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'opening_times', 'action' => 'edit', $openingTime['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'opening_times', 'action' => 'delete', $openingTime['id']), null, __('Are you sure you want to delete # %s?', $openingTime['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Opening Time'), array('controller' => 'opening_times', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
