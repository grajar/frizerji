<div class="saloons index">
	<h2><?php echo __('Saloons'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('interval'); ?></th>
			<th><?php echo $this->Paginator->sort('address'); ?></th>
			<th><?php echo $this->Paginator->sort('city'); ?></th>
			<th><?php echo $this->Paginator->sort('phone'); ?></th>
			<th><?php echo $this->Paginator->sort('active'); ?></th>
			<th><?php echo $this->Paginator->sort('latitude'); ?></th>
			<th><?php echo $this->Paginator->sort('longitude'); ?></th>
			<th><?php echo $this->Paginator->sort('logo'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($saloons as $saloon): ?>
	<tr>
		<td><?php echo h($saloon['Saloon']['id']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['name']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['interval']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['address']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['city']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['phone']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['active']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['latitude']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['longitude']); ?>&nbsp;</td>
		<td><?php echo h($saloon['Saloon']['logo']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $saloon['Saloon']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $saloon['Saloon']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $saloon['Saloon']['id']), null, __('Are you sure you want to delete # %s?', $saloon['Saloon']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Saloon'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Services'), array('controller' => 'services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Service'), array('controller' => 'services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Opening Times'), array('controller' => 'opening_times', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Opening Time'), array('controller' => 'opening_times', 'action' => 'add')); ?> </li>
	</ul>
</div>
