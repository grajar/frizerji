<div class="openingTimes index">
	<h2><?php echo __('Opening Times'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('saloon_id'); ?></th>
			<th><?php echo $this->Paginator->sort('open'); ?></th>
			<th><?php echo $this->Paginator->sort('close'); ?></th>
			<th><?php echo $this->Paginator->sort('day'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($openingTimes as $openingTime): ?>
	<tr>
		<td><?php echo h($openingTime['OpeningTime']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($openingTime['Saloon']['name'], array('controller' => 'saloons', 'action' => 'view', $openingTime['Saloon']['id'])); ?>
		</td>
		<td><?php echo h($openingTime['OpeningTime']['open']); ?>&nbsp;</td>
		<td><?php echo h($openingTime['OpeningTime']['close']); ?>&nbsp;</td>
		<td><?php echo h($openingTime['OpeningTime']['day']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $openingTime['OpeningTime']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $openingTime['OpeningTime']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $openingTime['OpeningTime']['id']), null, __('Are you sure you want to delete # %s?', $openingTime['OpeningTime']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Opening Time'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Saloons'), array('controller' => 'saloons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saloon'), array('controller' => 'saloons', 'action' => 'add')); ?> </li>
	</ul>
</div>
