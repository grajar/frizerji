<div class="openingTimes form">
<?php echo $this->Form->create('OpeningTime'); ?>
	<fieldset>
		<legend><?php echo __('Add Opening Time'); ?></legend>
	<?php
		echo $this->Form->input('saloon_id');
		echo $this->Form->input('open');
		echo $this->Form->input('close');
		echo $this->Form->input('day');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Opening Times'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Saloons'), array('controller' => 'saloons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Saloon'), array('controller' => 'saloons', 'action' => 'add')); ?> </li>
	</ul>
</div>
